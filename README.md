## Overview

This is a Simple C++ strategy game, start small end big :D


# Prerequisites

Before you proceed with building or contributing to this project, ensure you have the following software installed on your system:

- C++ compiler supporting C++11 or above
- CMake, version 3.10 or higher
- wxWidgets for GUI functionalities
- Catch2 for running unit tests

## Installing Dependencies

### C++ Compiler (GCC or Clang)

To install a C++ compiler such as GCC or Clang on Linux:

#### For GCC:

```bash
sudo apt-get install build-essential
```

### CMake

To install CMake:

```bash
sudo apt-get install cmake
```

# Build Instructions

## Clone the Repository

To get started with the project, you'll first need to clone the repository to your local machine. Follow these steps:

1. **Clone the Repository:** Open your terminal and execute the following command to clone the repository. Replace `repository-url` with the actual URL of the repository.

    ```bash
    git clone repository-url
    ```

2. **Navigate to the Repository Directory:** After cloning, navigate into the repository directory to access the project files.

    ```bash
    cd repository-name
    ```

Now, you should be inside the directory containing the project files.

## Configure and Build using CMake
1. **Create a Build Directory:** Navigate to the root directory of the project and create a new directory for the build.

    ```bash
    mkdir build
    ```

2. **Navigate to Build Directory:**

    ```bash
    cd build
    ```

3. **Run CMake:** Generate the Makefile using CMake.

    ```bash
    cmake ..
    ```

4. **Compile the Project:** Finally, compile the code.

    ```bash
    make
    ```

  This will build the project in the `build` directory.

# Running the Program

After successfully building the project, you'll find the executable in the `build` directory (or the directory where you've built the project). Follow these steps to run the program:

1. **Navigate to the Build Directory:** If you are not already there, navigate to the `build` directory where the executable is located.

    ```bash
    cd path/to/build/directory
    ```

2. **Run the Executable:** The executable's name will depend on how you've configured your project. Let's assume the executable is named `MythicalGameUnitBrowser`.

    ```bash
    ./game
    ```