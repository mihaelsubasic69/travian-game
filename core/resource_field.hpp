#pragma once

#include <iostream>
#include <string>
#include <map>

namespace core{

enum class ResourceType{
    CROP,
    IRON,
    LUMBER,
    CLAY
};

class ResourceField{
public:
    ResourceField() = default;
    ResourceField(ResourceType type);
    ResourceType type() const;
    uint64_t currentProduction() const;
    uint8_t level() const;
    std::map<std::string, uint64_t> upprageCost() const;
    void levelUp();
private:
    ResourceType type_;
    uint64_t currentProduction_ = 0;
    uint8_t level_ = 0;
    std::map<std::string, uint64_t> upprageCost_ = {
        {"Crop",100},
        {"Iron",100},
        {"Lumber",100},
        {"Clay",100}
    };
};

} // namespace core