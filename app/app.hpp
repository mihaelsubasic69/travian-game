#pragma once

#include <wx/wx.h>
#include "player_ui.hpp"
#include "login.hpp"

class App : public wxApp {
public:
  bool OnInit();
};