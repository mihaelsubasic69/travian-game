#include "player_ui.hpp"

namespace ui{

PlayerUI::PlayerUI(const wxString& title) : 
    wxFrame(nullptr,wxID_ANY,title,wxDefaultPosition,wxDefaultSize) {
        
    wxPanel* panel_ = new wxPanel(this, wxID_ANY);

    // background image
    wxBitmap backgroundBitmap(wxT("../ui/images/background_player_ui.png"), wxBITMAP_TYPE_PNG);
    wxStaticBitmap* background = new wxStaticBitmap(panel_, wxID_ANY, backgroundBitmap, wxDefaultPosition, wxDefaultSize);

    // top-buttons
    button_rank = new wxButton(panel_, wxID_ANY, "RANK", wxPoint(550, 10), wxSize(100, 30));
    button_map = new wxButton(panel_, wxID_ANY, "MAP", wxPoint(680, 10), wxSize(100, 30));

    // resource fields buttons
    lumber2 = new wxButton(panel_, wxID_ANY, "0", wxPoint(70, 160), wxSize(35, 35));
    lumber1 = new wxButton(panel_, wxID_ANY, "0", wxPoint(120, 160), wxSize(35, 35));
    lumber3 = new wxButton(panel_, wxID_ANY, "0", wxPoint(170, 160), wxSize(35, 35));
    lumber4 = new wxButton(panel_, wxID_ANY, "0", wxPoint(220, 160), wxSize(35, 35));

    clay1 = new wxButton(panel_, wxID_ANY, "0", wxPoint(70, 210), wxSize(35, 35));
    clay2 = new wxButton(panel_, wxID_ANY, "0", wxPoint(120, 210), wxSize(35, 35));
    clay3 = new wxButton(panel_, wxID_ANY, "0", wxPoint(170, 210), wxSize(35, 35));
    clay4 = new wxButton(panel_, wxID_ANY, "0", wxPoint(220, 210), wxSize(35, 35));

    iron1 = new wxButton(panel_, wxID_ANY, "0", wxPoint(70, 260), wxSize(35, 35));
    iron2 = new wxButton(panel_, wxID_ANY, "0", wxPoint(120, 260), wxSize(35, 35));
    iron3 = new wxButton(panel_, wxID_ANY, "0", wxPoint(170, 260), wxSize(35, 35));
    iron4 = new wxButton(panel_, wxID_ANY, "0", wxPoint(220, 260), wxSize(35, 35));

    crop1 = new wxButton(panel_, wxID_ANY, "0", wxPoint(70, 310), wxSize(35, 35));
    crop2 = new wxButton(panel_, wxID_ANY, "0", wxPoint(120, 310), wxSize(35, 35));
    crop3 = new wxButton(panel_, wxID_ANY, "0", wxPoint(170, 310), wxSize(35, 35));
    crop4 = new wxButton(panel_, wxID_ANY, "0", wxPoint(220, 310), wxSize(35, 35));
    
    // resource production per hour
    staticTxtLumber = new wxStaticText(panel_, wxID_ANY, "Lumber",wxPoint(5,170));
    staticTxtClay = new wxStaticText(panel_, wxID_ANY, "Clay",wxPoint(5,220));
    staticTxtIron = new wxStaticText(panel_, wxID_ANY, "Iron",wxPoint(5,270));
    staticTxtCrop = new wxStaticText(panel_, wxID_ANY, "Crop",wxPoint(5,320));

    staticTxtLumber->SetForegroundColour(*wxBLACK);
    staticTxtClay->SetForegroundColour(*wxBLACK);
    staticTxtIron->SetForegroundColour(*wxBLACK);
    staticTxtCrop->SetForegroundColour(*wxBLACK);

    // dataview available resources
    listctrl_res_ = new wxDataViewListCtrl(panel_, wxID_ANY, wxPoint(1000,150), wxSize(300,50));
    listctrl_res_->AppendTextColumn("Zito");
    listctrl_res_->AppendTextColumn("Drvo");
    listctrl_res_->AppendTextColumn("Zeljezo");
    listctrl_res_->AppendTextColumn("Glina");

    int colWidth = 75;
    for (int i = 0; i < 4; ++i)
    {
        wxDataViewColumn* column = listctrl_res_->GetColumn(i);
        column->SetWidth(colWidth);
    }

    wxVector<wxVariant> res_data;
    res_data.push_back("320");
    res_data.push_back("800");
    res_data.push_back("750");
    res_data.push_back("500");
    listctrl_res_->AppendItem(res_data);

    // dataview available units
    listctrl_unit_ = new wxDataViewListCtrl(panel_, wxID_ANY, wxPoint(1000,230), wxSize(300,50));
    listctrl_unit_->AppendTextColumn("Kopljanik");
    listctrl_unit_->AppendTextColumn("Falanga");
    listctrl_unit_->AppendTextColumn("Imperijalac");

    colWidth = 100;
    for (int i = 0; i < 3; ++i)
    {
        wxDataViewColumn* column = listctrl_unit_->GetColumn(i);
        column->SetWidth(colWidth);
    }

    wxVector<wxVariant> unit_data;
    unit_data.push_back("10");
    unit_data.push_back("5");
    unit_data.push_back("3");
    listctrl_unit_->AppendItem(unit_data);

    // pseudo server time 
    staticTxtTime = new wxStaticText(panel_, wxID_ANY, "", wxPoint(30,780));
    staticTxtTime->SetForegroundColour(*wxRED);

    timer.Bind(wxEVT_TIMER, &PlayerUI::OnTimer, this);
    timer.Start(1000); // 1 second interval
}

void PlayerUI::OnTimer([[maybe_unused]] wxTimerEvent& event) {
    wxDateTime now = wxDateTime::Now();
    staticTxtTime->SetLabel(now.FormatTime());
}

} // namspace ui