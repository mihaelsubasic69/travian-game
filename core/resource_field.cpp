#include "resource_field.hpp"

namespace core{

    ResourceField::ResourceField(ResourceType type): type_(type){}

    ResourceType ResourceField::type() const{
        return type_;
    }

    uint64_t ResourceField::currentProduction() const{
        return currentProduction_;
    }

    uint8_t ResourceField::level() const{
        return level_;
    }

    std::map<std::string, uint64_t> ResourceField::upprageCost() const{
        return upprageCost_;
    }

    void ResourceField::levelUp(){
        currentProduction_ += 100;
        level_++;
        for(auto& e: upprageCost_){
            e.second += 100;
        }
    }

}