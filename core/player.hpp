#pragma once

#include <iostream>
#include <vector>
#include <map>
#include "unit.hpp"
#include "resource_field.hpp"

enum class Nation{
    ROMAN,
    GREEK,
    GAUL
};

namespace core{

class Player{
public:
    Player() = default;
    Player(std::string name, Nation nation);
    std::string name() const;
    uint64_t points() const;
    uint64_t gold() const;
    uint64_t score() const;
    Nation nation() const;
    std::vector<Unit> units() const;
    std::map<std::string, uint64_t> resources() const;
    std::vector<ResourceField> resourceFields() const;
private:
    std::string name_;
    uint64_t points_ = 0;
    uint64_t gold_ = 20;
    uint64_t score_ = 0;
    Nation nation_;
    std::vector<Unit> units_;
    std::map<std::string, uint64_t> resources_ = {
        {"Crop",800},
        {"Iron",800},
        {"Lumber",800},
        {"Clay",800}
    };
    std::vector<ResourceField> resourceFields_;
};

} // namespace core