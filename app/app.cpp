#include "app.hpp"

bool App::OnInit() {
    ui::PlayerUI* game = new ui::PlayerUI(wxT("Welcome to Travian 2.0"));
    game->SetClientSize(1400,800);
    game->Center();
    game->Show();
    return true;
}

wxIMPLEMENT_APP(App);