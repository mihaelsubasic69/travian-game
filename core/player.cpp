#include "player.hpp"

namespace core{

Player::Player(std::string name, Nation nation):
    name_(name), nation_(nation){
    for(int i = 0; i < 16; i++){
        if(i < 4)
            resourceFields_.push_back(ResourceField(ResourceType::CROP));
        if(i >= 4 and i < 8)
            resourceFields_.push_back(ResourceField(ResourceType::LUMBER));
        if(i >= 8 and i < 12)
            resourceFields_.push_back(ResourceField(ResourceType::IRON));
        if(i >= 12 and i < 16)
            resourceFields_.push_back(ResourceField(ResourceType::CLAY));
    }
}

std::string Player::name() const{
    return name_;
}

uint64_t Player::points() const{
    return points_;
}

uint64_t Player::gold() const{
    return gold_;
}
uint64_t Player::score() const{
    return score_;
}

Nation Player::nation() const{
    return nation_;
}

std::vector<Unit> Player::units() const{
    return units_;
}

std::map<std::string, uint64_t> Player::resources() const{
    return resources_;
}

std::vector<ResourceField> Player::resourceFields() const{
    return resourceFields_;
}


} // namespace core