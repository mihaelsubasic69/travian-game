#pragma once

#include <wx/wx.h>
#include <wx/dataview.h>

namespace ui{ 

class PlayerUI: public wxFrame {
public:
    PlayerUI(const wxString& title);

private:
    wxPanel* panel_ = nullptr;
    
    // resource production per hour
    wxStaticText* staticTxtLumber = nullptr;
    wxStaticText* staticTxtClay = nullptr;
    wxStaticText* staticTxtIron = nullptr;
    wxStaticText* staticTxtCrop = nullptr;

    // top-buttons
    wxButton* button_rank = nullptr;
    wxButton* button_map = nullptr;

    // resource fields buttons
    wxButton* lumber1 = nullptr;
    wxButton* lumber2 = nullptr;
    wxButton* lumber3 = nullptr;
    wxButton* lumber4 = nullptr;

    wxButton* clay1 = nullptr;
    wxButton* clay2 = nullptr;
    wxButton* clay3 = nullptr;
    wxButton* clay4 = nullptr;

    wxButton* iron1 = nullptr;
    wxButton* iron2 = nullptr;
    wxButton* iron3 = nullptr;
    wxButton* iron4 = nullptr;

    wxButton* crop1 = nullptr;
    wxButton* crop2 = nullptr;
    wxButton* crop3 = nullptr;
    wxButton* crop4 = nullptr;

    // dataview available resources
    wxDataViewListCtrl* listctrl_res_ = nullptr;

    // dataview available units
    wxDataViewListCtrl* listctrl_unit_ = nullptr;

    // pseudo server time 
    wxStaticText* staticTxtTime = nullptr;
    wxTimer timer;
    void OnTimer([[maybe_unused]] wxTimerEvent& event);

};

} // ui